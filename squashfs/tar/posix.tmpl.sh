#!/usr/bin/env bash

# Strict shell
set -o errexit
set -o nounset
set -o pipefail

# Bazel substitutions
SQFSTAR="{{sqfstar}}"
CAT="{{cat}}"
GZIP="{{gzip}}"
BZIP2="{{bzip2}}"
XZ="{{xz}}"
ZSTD="{{zstd}}"
readonly SQFSTAR CAT GZIP BZIP2 XZ ZSTD

# Runfiles location
function rlocation() (
  # TODO: handle runfiles
  local FILEPATH="${1}"
  test -f "${FILEPATH}"
  echo "${FILEPATH}"
)

# Process command line arguments
declare -a TARS
while true; do
  case "${1}" in
  *".tar" | *".tar.gz" | *".tar.bz2" | *".tar.xz" | *".tar.zst")
    TARS+=("$(rlocation "${1}")")
    shift
    continue
    ;;
  *)
    break
    ;;
  esac
done

# Perform the difference
{
  for TAR in "${TARS[@]}"; do
    case "${TAR}" in
    *".tar")
      "${CAT}" "${TAR}"
      ;;
    *".tar.gz")
      "${GZIP}" -fcd "${TAR}"
      ;;
    *".tar.bz2")
      "${BZIP2}" -fcd "${TAR}"
      ;;
    *".tar.xz")
      "${XZ}" -fcd "${TAR}"
      ;;
    *".tar.zst")
      "${ZSTD}" -fcd "${TAR}"
      ;;
    *)
      echo >&2 "Unsupported: ${TAR}"
      exit 2
      ;;
    esac
  done
} | "${SQFSTAR}" "${@}"
