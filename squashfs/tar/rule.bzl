load("//squashfs/compression/gzip:defs.bzl", "SquashfsCompressionGzipInfo")
load("//squashfs/compression/lz4:defs.bzl", "SquashfsCompressionLz4Info")
load("//squashfs/compression/zstd:defs.bzl", "SquashfsCompressionZstdInfo")

visibility("//...")

DOC = """Generates a SquashFS from a collection of `.tar` archives.

```py
squashfs_tar(
    name = "squashed",
    srcs = [
        ":some-tar-target",
        ":some-tar-file.tar",
    ]
)
```
"""

ATTRS = {
    "srcs": attr.label_list(
        doc = "The `.tar` archives to squashify.",
        allow_files = [".tar", ".tar.gz", ".tar.bz2", ".tar.xz", ".tar.zst"],
        mandatory = True,
    ),
    "compression": attr.label(
        doc = "The directory to squashify.",
        default = "//squashfs/compression:gzip",
        providers = [
            [SquashfsCompressionGzipInfo],
            [SquashfsCompressionLz4Info],
            [SquashfsCompressionZstdInfo],
        ],
    ),
    "exclude": attr.label(
        doc = "Files or directories to exclude, one per line.",
        allow_single_file = True,
    ),
    "template": attr.label(
        doc = "The script template to be rendered.",
        allow_single_file = True,
        default = ":template",
    ),
}

def _resources(os, inputs):
    return {"cpu": 4, "memory": 1024}

def implementation(ctx):
    sqfstar = ctx.toolchains["//squashfs/toolchain/sqfstar:type"]
    cat = ctx.toolchains["@rules_coreutils//coreutils/toolchain/cat:type"]
    gzip = ctx.toolchains["@rules_gzip//gzip/toolchain/gzip:type"]
    bzip2 = ctx.toolchains["@rules_bzip2//bzip2/toolchain/bzip2:type"]
    xz = ctx.toolchains["@rules_xz//xz/toolchain/xz:type"]
    zstd = ctx.toolchains["@rules_zstd//zstd/toolchain/zstd:type"]

    squashed = ctx.actions.declare_file("{}.squashfs".format(ctx.label.name))

    inputs = depset(transitive = [s[DefaultInfo].files for s in ctx.attr.srcs])

    args = ctx.actions.args()
    args.add_all(inputs)
    args.add("-exit-on-error")
    args.add("-no-progress")
    args.add("-quiet")
    args.add("-reproducible")
    args.add("-mkfs-time").add("0")
    args.add("-root-time").add("0")

    resources = _resources(None, 1)
    args.add("-processors").add(resources["cpu"])
    args.add("-mem").add("{}M".format(resources["memory"]))

    if SquashfsCompressionGzipInfo in ctx.attr.compression:
        args.add_all(ctx.attr.compression[SquashfsCompressionGzipInfo].arguments)
    if SquashfsCompressionLz4Info in ctx.attr.compression:
        args.add_all(ctx.attr.compression[SquashfsCompressionLz4Info].arguments)
    if SquashfsCompressionZstdInfo in ctx.attr.compression:
        args.add_all(ctx.attr.compression[SquashfsCompressionZstdInfo].arguments)

    args.add("-default-mode").add("0755")
    args.add("-default-uid").add("0")
    args.add("-default-gid").add("0")
    args.add("-ignore-zeros")

    # TODO: do we actually want this or `-force-{u,g}id`?
    args.add("-all-root")

    if ctx.file.exclude:
        args.add("-ef").add(ctx.file.exclude.path)
        inputs = depset([ctx.file.exclude], transitive = [inputs])

    args.add(squashed.path)

    rendered = ctx.actions.declare_file("{}.rendered.{}".format(ctx.label.name, ctx.file.template.extension))
    substitutions = ctx.actions.template_dict()
    substitutions.add("{{sqfstar}}", sqfstar.executable.path)
    substitutions.add("{{cat}}", cat.executable.path)
    substitutions.add("{{gzip}}", gzip.executable.path)
    substitutions.add("{{bzip2}}", bzip2.executable.path)
    substitutions.add("{{xz}}", xz.executable.path)
    substitutions.add("{{zstd}}", zstd.executable.path)
    ctx.actions.expand_template(
        output = rendered,
        template = ctx.file.template,
        is_executable = True,
        computed_substitutions = substitutions,
    )
    inputs = depset([rendered], transitive = [inputs])

    ctx.actions.run(
        outputs = [squashed],
        inputs = inputs,
        executable = rendered,
        tools = [sqfstar.run, cat.run, gzip.run, bzip2.run, xz.run, zstd.run],
        arguments = [args],
        mnemonic = "SquashfsTar",
        progress_message = "squashing tars into %{output}",
        resource_set = _resources,
    )

    return DefaultInfo(files = depset([squashed]))

tar = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = [
        "@rules_coreutils//coreutils/toolchain/cat:type",
        "@rules_gzip//gzip/toolchain/gzip:type",
        "@rules_bzip2//bzip2/toolchain/bzip2:type",
        "@rules_xz//xz/toolchain/xz:type",
        "@rules_zstd//zstd/toolchain/zstd:type",
        "//squashfs/toolchain/sqfstar:type",
    ],
)
