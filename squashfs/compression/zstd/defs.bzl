load(":rule.bzl", _zstd = "zstd")
load(":ZstdInfo.bzl", _ZstdInfo = "ZstdInfo")

visibility("public")

squashfs_compression_zstd = _zstd
SquashfsCompressionZstdInfo = _ZstdInfo
