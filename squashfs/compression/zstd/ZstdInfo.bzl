visibility("//...")

def init(level = 9, window = 15, strategy = "default"):
    """
    Initializes a `ZstdInfo` provider.

    Args:
      level: The compression level.
      window: The window size.
      strategy: One of `default`, `filtered`, `huffman_only`, `run_length_encoded` and `fixed`

    Returns:
      A mapping of keywords for the `zstd_info` raw constructor.
    """
    if level < 1 or level > 22:
        fail("`ZstdInfo.level` must be [0, 22]: {}".format(level))

    arguments = (
        "-comp",
        "zstd",
        "-Xcompression-level",
        str(level),
    )

    return {
        "level": level,
        "arguments": arguments,
    }

ZstdInfo, zstd_info = provider(
    "`zstd` compression options for `squashfs`.",
    fields = ("level", "arguments"),
    init = init,
)
