load(":ZstdInfo.bzl", "ZstdInfo")

visibility("//...")

DOC = """Creates a `zstd` compression provider.

```py
squashfs_compression_zstd(
    name = "zstd",
    level = 22,
)

squashfs_directory(
    name = "squashed",
    src = ":directory",
    compression = ":zstd",
)
```
"""

ATTRS = {
    "level": attr.int(
        doc = "The compression level.",
        default = 22,
        values = [l for l in range(1, 23)],
    ),
}

def implementation(ctx):
    return ZstdInfo(
        level = ctx.attr.level,
    )

zstd = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    provides = [ZstdInfo],
)
