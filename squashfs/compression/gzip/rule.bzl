load(":GzipInfo.bzl", "GzipInfo")

visibility("//...")

DOC = """Creates a `gzip` compression provider.

```py
squashfs_compression_gzip(
    name = "gzip",
    level = 9,
    window = 15,
    strategy = "run_length_encoded",
)

squashfs_directory(
    name = "squashed",
    src = ":directory",
    compression = ":gzip",
)
```
"""

ATTRS = {
    "level": attr.int(
        doc = "The compression level.",
        default = 9,
        values = [1, 2, 3, 4, 5, 6, 7, 8, 9],
    ),
    "window": attr.int(
        doc = "The window size.",
        default = 15,
        values = [8, 9, 10, 11, 12, 13, 14, 15],
    ),
    "strategy": attr.string(
        doc = "The window size.",
        values = ["default", "filtered", "huffman_only", "run_length_encoded", "fixed"],
    ),
}

def implementation(ctx):
    return GzipInfo(
        level = ctx.attr.level,
        window = ctx.attr.window,
        strategy = ctx.attr.strategy,
    )

gzip = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    provides = [GzipInfo],
)
