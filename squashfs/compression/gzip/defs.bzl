load(":rule.bzl", _gzip = "gzip")
load(":GzipInfo.bzl", _GzipInfo = "GzipInfo")

visibility("public")

squashfs_compression_gzip = _gzip
SquashfsCompressionGzipInfo = _GzipInfo
