visibility("//...")

def init(level = 9, window = 15, strategy = None):
    """
    Initializes a `GzipInfo` provider.

    Args:
      level: The compression level.
      window: The window size.
      strategy: One of `default`, `filtered`, `huffman_only`, `run_length_encoded` and `fixed`

    Returns:
      A mapping of keywords for the `gzip_info` raw constructor.
    """
    if level not in (1, 2, 3, 4, 5, 6, 7, 8, 9):
        fail("`GzipInfo.level` must be [0, 9]: {}".format(level))

    if window not in (8, 9, 10, 11, 12, 13, 14, 15):
        fail("`GzipInfo.window` must be [8, 15]: {}".format(window))

    if strategy and strategy not in ("default", "filtered", "huffman_only", "run_length_encoded", "fixed"):
        fail("`GzipInfo.strategy` must be on of `default`, `filtered`, `huffman_only`, `run_length_encoded` and `fixed`: {}".format(strategy))

    arguments = (
        "-comp",
        "gzip",
        "-Xcompression-level",
        str(level),
        "-Xwindow-size",
        str(window),
    )

    if strategy:
        arguments += (
            "-Xstrategy",
            strategy,
        )

    return {
        "level": level,
        "window": window,
        "strategy": strategy,
        "arguments": arguments,
    }

GzipInfo, gzip_info = provider(
    "`gzip` compression options for `squashfs`.",
    fields = ("level", "window", "strategy", "arguments"),
    init = init,
)
