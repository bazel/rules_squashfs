visibility("//...")

def init(compression = "default"):
    """
    Initializes a `Lz4Info` provider.

    Args:
      compression: One of `default` or `high`.

    Returns:
      A mapping of keywords for the `lz4_info` raw constructor.
    """
    if compression not in ("default", "high"):
        fail("`Lz4Info.strategy` must be on of `default`, `high`: {}".format(compression))

    arguments = (
        "-comp",
        "lz4",
    )

    if compression == "high":
        arguments += ("-Xhc",)

    return {
        "compression": compression,
        "arguments": arguments,
    }

Lz4Info, lz4_info = provider(
    "`lz4` compression options for `squashfs`.",
    fields = ("compression", "arguments"),
    init = init,
)
