load(":Lz4Info.bzl", "Lz4Info")

visibility("//...")

DOC = """Creates a `lz4` compression provider.

```py
squashfs_compression_lz4(
    name = "lz4",
    compression = "high",
)

squashfs_directory(
    name = "squashed",
    src = ":directory",
    compression = ":lz4",
)
```
"""

ATTRS = {
    "compression": attr.string(
        doc = "The compression amount.",
        default = "high",
        values = ["default", "high"],
    ),
}

def implementation(ctx):
    return Lz4Info(
        compression = ctx.attr.compression,
    )

lz4 = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    provides = [Lz4Info],
)
