load(":rule.bzl", _lz4 = "lz4")
load(":Lz4Info.bzl", _Lz4Info = "Lz4Info")

visibility("public")

squashfs_compression_lz4 = _lz4
SquashfsCompressionLz4Info = _Lz4Info
