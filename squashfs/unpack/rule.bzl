visibility("//...")

DOC = """Unpacks a SquashFS into a declared directory.

```py
squashfs_unpack(
    name = "unpacked",
    src = ":squashed",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "The directory to squashify.",
        allow_single_file = True,
        mandatory = True,
    ),
    "extract": attr.label(
        doc = "Files or directories to extract, one per line.",
        allow_single_file = True,
    ),
    "exclude": attr.label(
        doc = "Files or directories to exclude, one per line.",
        allow_single_file = True,
    ),
}

def _resources(os, inputs):
    return {"cpu": 4}

def implementation(ctx):
    unsquashfs = ctx.toolchains["//squashfs/toolchain/unsquashfs:type"]

    unpacked = ctx.actions.declare_directory("{}.unpacked".format(ctx.label.name))

    inputs = [ctx.file.src]

    args = ctx.actions.args()
    args.add("-dest").add(unpacked.path)
    args.add("-no-progress")
    args.add("-quiet")
    args.add("-follow-symlinks")

    # TODO: why does this always error?
    #args.add("-missing-symlinks")
    args.add("-strict-errors")

    resources = _resources(None, 1)
    args.add("-processors").add(resources["cpu"])

    if ctx.file.exclude:
        args.add("-exclude-file").add(ctx.file.exclude.path)
        inputs.append(ctx.file.exclude)

    if ctx.file.extract:
        args.add("-extract-file").add(ctx.file.extract.path)
        inputs.append(ctx.file.extract)

    args.add(ctx.file.src)

    ctx.actions.run(
        outputs = [unpacked],
        inputs = inputs,
        executable = unsquashfs.run,
        arguments = [args],
        mnemonic = "UnsquashfsDirectory",
        progress_message = "unsquashing %{input} into %{output}",
        resource_set = _resources,
    )

    return DefaultInfo(files = depset([unpacked]))

unpack = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//squashfs/toolchain/unsquashfs:type"],
)
