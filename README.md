# `rules_squashfs`

> A ruleset for working with compressed read-only filesystem files.

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(name="rules_squashfs", version="0.0.0")
```

## Hermeticity

The module uses `squashfs-tools` from the [BCR][bcr]. This requires a C/C++ toolchain. Testing is performed with `hermetic_cc_toolchain`.

Otherwise, everything else is hermetic, no Bash is required on Windows.

## Release Registry

The project publishes the relevant files to GitLab releases for use when a version has not been added to the upstream [BCR][bcr].

This is often the case for pre-release versions.

Add the following to `.bazelrc`:

```
# `bzlmod` pre-release registries
common --registry https://bcr.bazel.build
common --registry=https://gitlab.arm.com/bazel/rules_squashfs/-/releases/v1.0.0-alpha.1/downloads
```

Then a GitLab release version can be used in `bazel_dep`.

[bcr]: https://registry.bazel.build/
